'''
5. Top players per season based on no of man of the match titles.
'''

import csv
from matplotlib import pyplot as plt


def get_top_players_of_matches(season):
    matches_reader = csv.DictReader(open('Data files/matches.csv'))
    players_of_matches = {}
    for match in matches_reader:
        if match['season'] == season:
            if match['player_of_match'] in players_of_matches:
                players_of_matches[match['player_of_match']] += 1
            else:
                players_of_matches[match['player_of_match']] = 1
            players_of_matches = dict(sorted(players_of_matches.items(), key=lambda x: x[1])[::-1])
    return dict(list(players_of_matches.items())[:10])


def plot(season):
    players = get_top_players_of_matches(season)
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.bar(players.keys(), players.values())
    ax1.set(title="Top 10 players of {0} (based on number of man of the match title)".format(season), xlabel="Player", ylabel="No. of man of the match title")
    plt.tight_layout()
    plt.show()


def menu():
    print(
        '''
                             IPL Data Info\n
        Top 10 batsman based on their no of man of the match titles.

        Select a season to know top 10 players...

        1 : 2008
        2 : 2009
        3 : 2010
        4 : 2011
        5 : 2012
        6 : 2013
        7 : 2014
        8 : 2015
        9 : 2016
        10 : 2017
        '''
    )
    choice = input('Enter choice : ')

    if choice == '1':
        plot('2008')
    elif choice == '2':
        plot('2009')
    elif choice == '3':
        plot('2010')
    elif choice == '4':
        plot('2011')
    elif choice == '5':
        plot('2012')
    elif choice == '6':
        plot('2013')
    elif choice == '7':
        plot('2014')
    elif choice == '8':
        plot('2015')
    elif choice == '9':
        plot('2016')
    elif choice == '10':
        plot('2017')
    else:
        print('Invalid Choice')

menu()
