'''
3. For the year 2016 plot the extra runs conceded per team.
'''

import csv
import matplotlib.pyplot as plt


def get_match_id():
    matches_reader = csv.DictReader(open('Data files/matches.csv'))
    match_id = []
    for match in matches_reader:
        if match['season'] == '2016':
            match_id.append(match['id'])
    return match_id


def get_extra_runs():
    deliveries_reader = csv.DictReader(open('Data files/deliveries.csv'))
    extra_runs_conceded = {}
    match_id = get_match_id()
    for delivery in deliveries_reader:
        if delivery['match_id'] in match_id:
            if delivery['bowling_team'] not in extra_runs_conceded:
                extra_runs_conceded[delivery['bowling_team']] = int(delivery['extra_runs'])
            else:
                extra_runs_conceded[delivery['bowling_team']] += int(delivery['extra_runs'])
    return extra_runs_conceded


def plot():
    extra_runs = get_extra_runs()
    plt.bar(extra_runs.keys(), extra_runs.values())
    plt.title("Extra runs conceded per team in 2016")
    plt.xlabel("Teams")
    plt.ylabel("Runs")
    plt.tight_layout()
    plt.show()

plot()
