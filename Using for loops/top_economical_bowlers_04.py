'''
4. For the year 2015 plot the top economical bowlers.
'''
import csv
from matplotlib import pyplot as plt


def get_match_id():
    matches_reader = csv.DictReader(open('Data files/matches.csv'))
    match_id = []
    for match in matches_reader:
        if match['season'] == '2015':
            match_id.append(match['id'])
    return match_id


def get_bowlers_data():
    deliveries_reader = csv.DictReader(open('Data files/deliveries.csv'))
    bowls = {}
    match_id = get_match_id()
    for delivery in deliveries_reader:
        if delivery['match_id'] in match_id:
            if delivery['bowler'] in bowls:
                if (delivery['noball_runs'] == '0' and delivery['wide_runs'] == '0'):
                    bowls[delivery['bowler']][0] += 1
                bowls[delivery['bowler']][1] += int(delivery['total_runs']) - int(delivery['bye_runs']) - int(delivery['legbye_runs'])
            else:
                bowls[delivery['bowler']] = [1, int(delivery['total_runs']) - int(delivery['bye_runs']) - int(delivery['legbye_runs'])]
    return bowls


def get_economy():
    economy = {}
    bowlers = get_bowlers_data()
    for bowler in bowlers:
        total_overs = bowlers[bowler][0]/6
        economy[bowler] = round(bowlers[bowler][1]/total_overs, 2)
    economy = sorted(economy.items(), key=lambda x: x[1])[:10]
    return dict(economy)


def plot():
    top10_economic_bowlers = get_economy()
    plt.bar(top10_economic_bowlers.keys(), top10_economic_bowlers.values())
    plt.title("Top 10 Economical bowlers")
    plt.xlabel("Bowlers")
    plt.ylabel("Economy")
    plt.show()

plot()
