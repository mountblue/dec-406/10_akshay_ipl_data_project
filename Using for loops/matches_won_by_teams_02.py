'''
2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.
'''
import csv
from matplotlib import pyplot as plt


def get_matches_won():
    matches_reader = csv.DictReader(open('Data files/matches.csv'))
    matches_won = {}
    for match in matches_reader:
        season = match['season']
        winner = match['winner']
        if winner:
            if season not in matches_won:
                matches_won[season] = {}
            if winner not in matches_won[season]:
                matches_won[season][winner] = 1
            else:
                matches_won[season][winner] += 1
    return matches_won


def get_team_colors():
    return {'Chennai Super Kings': 'y', 'Deccan Chargers': 'Teal', 'Delhi Daredevils': 'Maroon', 'Gujarat Lions': 'Aqua', 'Kings XI Punjab': 'r', 'Kochi Tuskers Kerala': 'c', 'Kolkata Knight Riders': 'Navy', 'Mumbai Indians': 'b', 'Pune Warriors': 'g', 'Rajasthan Royals': 'Silver', 'Rising Pune Supergiant': 'Purple', 'Royal Challengers Bangalore': 'Lime', 'Sunrisers Hyderabad': 'm'}


def get_sorted_data():
    teams_colors = get_team_colors()
    won_matches = get_matches_won()
    for team in teams_colors:
        for teams in won_matches:
            if team not in won_matches[teams]:
                won_matches[teams][team] = 0

    won_matches = dict(sorted(won_matches.items(), key=lambda x: x[0]))
    for teams in won_matches:
        won_matches[teams] = sorted(won_matches[teams].items(), key=lambda x: x[0])
        won_matches[teams] = dict(won_matches[teams])
    return sorted(won_matches.items())


def plot():
    teams_colors = get_team_colors()
    won_matches = get_sorted_data()
    for teams in won_matches:
        year = teams[0]
        team = teams[1]
        last_stack = 0
        for team_name, num_matches in team.items():
            plt.bar(year, num_matches, color=teams_colors[team_name], bottom=last_stack)
            last_stack += num_matches
            plt.legend(teams_colors.keys())
    plt.title('Matches won by all teams over all the years of IPL')
    plt.xlabel('Year')
    plt.ylabel('No of matches')
    plt.show()

plot()
