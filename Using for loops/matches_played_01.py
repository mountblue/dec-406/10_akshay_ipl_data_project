'''
1. Plot the number of matches played per year of all the years in IPL.
'''

import csv
from matplotlib import pyplot as plt


def get_matches_played():
    matches_reader = csv.DictReader(open('Data files/matches.csv'))
    matches_per_year = {}
    for match in matches_reader:
        if match['season'] not in matches_per_year:
            matches_per_year[match['season']] = 1
        else:
            matches_per_year[match['season']] += 1
    matches_per_year = sorted(matches_per_year.items(), key=lambda x: x[0])
    return dict(matches_per_year)


def plot():
    matches_played = get_matches_played()
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.bar(matches_played.keys(), matches_played.values())
    ax1.set(title="Matches played per year", xlabel="Years", ylabel="No. of matches")
    plt.show()

plot()
