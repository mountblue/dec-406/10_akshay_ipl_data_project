'''
2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.
'''

import mysql.connector


def get_matches_won():
    try:
        mysql_connection = mysql.connector.connect(host='localhost', database='mysql', user='akshay', password='incorrect')
        cursor = mysql_connection.cursor()
        sql_query = "SELECT season,winner,count(winner) AS 'num of matches' FROM matches GROUP BY season,winner;"
        cursor.execute(sql_query)
        matches_won = cursor.fetchall()
        print("Matches won : \n")
        print('Year\t Team\t Wins\n')
        for match in matches_won:
            print("{}\t {}\t {}\n".format(match[0], match[1], match[2]))
    except mysql.connector.Error as error:
        print("Failed to get record from database: {}".format(error))
    finally:
        if mysql_connection.is_connected():
            cursor.close()
            mysql_connection.close()
            print("\nConnection is closed")

get_matches_won()
