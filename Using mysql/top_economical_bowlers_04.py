'''
4. For the year 2015 plot the top economical bowlers.
'''
import mysql.connector


def get_top_bowlers():
    try:
        mysql_connection = mysql.connector.connect(host='localhost', database='mysql', user='akshay', password='incorrect')
        sql_query = "SELECT bowler as Bowler, ((SUM(total_runs)-SUM(bye_runs)-SUM(legbye_runs))/((COUNT(ball)-SUM(CASE WHEN noball_runs=0 THEN 0 ELSE 1 END)-SUM(CASE WHEN wide_runs=0 THEN 0 ELSE 1 END))/6)) AS Economy FROM deliveries INNER JOIN matches ON deliveries.match_id=matches.id WHERE season=2015 GROUP BY bowler ORDER BY Economy LIMIT 10;"
        cursor = mysql_connection.cursor()
        cursor.execute(sql_query)
        top_bowlers = cursor.fetchall()
        print("Bowler : Economy\n")
        for bowler in top_bowlers:
            print("{} : {}".format(bowler[0], bowler[1]))
    except mysql.connector.Error as error:
        print("Failed to get record from database: {}".format(error))
    finally:
        if mysql_connection.is_connected():
            cursor.close()
            mysql_connection.close()
            print('\nConnection is closed')

get_top_bowlers()
