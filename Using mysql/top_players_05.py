'''
5. Top players per season based on no of man of the match titles.
'''

import mysql.connector


def get_season_top_players(season):
    try:
        mysql_connection = mysql.connector.connect(host='localhost', database='mysql', user='akshay', password='incorrect')
        cursor = mysql_connection.cursor()
        sql_query = """SELECT season, player_of_match AS 'Player of Match', count(player_of_match) AS 'No of titles' FROM matches GROUP BY season,player_of_match HAVING season= %s ORDER BY count(player_of_match) DESC LIMIT 10;"""
        cursor.execute(sql_query, (season, ))
        top_players = cursor.fetchall()
        print("Top players in {} (based on no. of player of the match titles) \n".format(season))
        for player in top_players:
            print("{} : {}".format(player[1], player[2]))
    except mysql.connector.Error as error:
        print("Failed to get record from database: {}".format(error))
    finally:
        if mysql_connection.is_connected():
            cursor.close()
            mysql_connection.close()
            print("connection is closed")


def menu():
    print(
        '''
                             IPL Data Info\n
        Top 10 batsman based on their no of man of the match titles.

        Select a season to know top 10 players...

        1 : 2008
        2 : 2009
        3 : 2010
        4 : 2011
        5 : 2012
        6 : 2013
        7 : 2014
        8 : 2015
        9 : 2016
        10 : 2017
        '''
    )
    choice = input('Enter choice : ')

    if choice == '1':
        get_season_top_players('2008')
    elif choice == '2':
        get_season_top_players('2009')
    elif choice == '3':
        get_season_top_players('2010')
    elif choice == '4':
        get_season_top_players('2011')
    elif choice == '5':
        get_season_top_players('2012')
    elif choice == '6':
        get_season_top_players('2013')
    elif choice == '7':
        get_season_top_players('2014')
    elif choice == '8':
        get_season_top_players('2015')
    elif choice == '9':
        get_season_top_players('2016')
    elif choice == '10':
        get_season_top_players('2017')
    else:
        print('Invalid Choice')

menu()
