'''
3. For the year 2016 plot the extra runs conceded per team.
'''
import mysql.connector


def get_extra_runs():
    try:
        mysql_connection = mysql.connector.connect(host='localhost', database='mysql', user='akshay', password='incorrect')
        cursor = mysql_connection.cursor()
        sql_query = "SELECT bowling_team as Team,SUM(extra_runs) AS runs_conceded FROM deliveries INNER JOIN matches ON deliveries.match_id = matches.id WHERE matches.season=2016 GROUP BY bowling_team;"
        cursor.execute(sql_query)
        extra_runs = cursor.fetchall()
        print("Extra Runs conceded : \n")
        for team in extra_runs:
            print("{} : {}".format(team[0], team[1]))
    except mysql.connector.Error as error:
        print("Failed to get record from database: {}".format(error))
    finally:
        if mysql_connection.is_connected():
            cursor.close()
            mysql_connection.close()
            print("\nConnection is closed")

get_extra_runs()
