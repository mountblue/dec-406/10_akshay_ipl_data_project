'''
1. Plot the number of matches played per year of all the years in IPL.
'''
import mysql.connector


def get_matches_played():
    try:
        mysql_connection = mysql.connector.connect(host='localhost', database='mysql', user='akshay', password='incorrect')
        cursor = mysql_connection.cursor()
        sql_query = "SELECT season AS Years,COUNT(season) AS 'No of Matches' FROM matches GROUP BY season;"
        cursor.execute(sql_query)
        matches_played = cursor.fetchall()
        print("Matches Played : \n")
        for match in matches_played:
            print("{} : {}".format(match[0], match[1]))
    except mysql.connector.Error as error:
        print("Failed to get record from database: {}".format(error))
    finally:
        if mysql_connection.is_connected():
            cursor.close()
            mysql_connection.close()
            print("\nConnection is closed")

get_matches_played()
