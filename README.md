# Data Project

Python functions that will transform the raw csv data into a data structure in a format suitable for plotting with matplotlib.

# Generate the following plots ...

1. Get number of matches played per year of all the years in IPL and also plot using matplotlib.
2. Get matches won of all teams over all the years of IPL and also plot stacked bar graph using matplotlib.
3. For the year 2016 plot the extra runs conceded per team.
4. For the year 2015 plot the top economical bowlers.
5. For all seasons, get top 10 players (based on number of player of the match titles) depending on user input.